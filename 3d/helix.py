import primitives
import perspective
import orthogonal
import numpy as np
import renderer

# generate cubes in helix
numCubes = 31
distance = 1.5
helixRad = 3

cubeLen = 1.7

cubes = []

for angle in np.linspace(0, distance*(2*np.pi), num=numCubes):
    x = (2*helixRad * np.sin(angle)) + 5
    y = (helixRad * np.cos(angle)) + 2.5
    z = (2*np.pi) * angle + 2
    tmpCube = primitives.cube(cubeLen, [x,y,z])
    cubes.append(tmpCube)
    
loopTime = 2
fps = 60
    
anim = []
    
for angle in np.linspace(0, 2*np.pi, num=loopTime*fps):
    frame = []
    for cube_index in range(len(cubes)):
        offset = cube_index * (len(cubes)/(2*np.pi))
        cube = cubes[cube_index]
        cube.rotX((2*np.pi)/(loopTime*fps))
        cube.rotY((2*np.pi)/(loopTime*fps))
        x = (2*helixRad * np.sin(angle + offset)) + 5
        y = (helixRad * np.cos(angle + offset)) + 2.5
        #z = (2*np.pi) * angle + 2
        z = cube.z
        cube.move([x,y,z])
        for polygon in cube.originPolygons:
            frame.append(polygon)
        
    anim.append(frame)

print(anim)

anim = perspective.allFrames(anim)

renderer.playAnim(anim, 360, 90, fps)
