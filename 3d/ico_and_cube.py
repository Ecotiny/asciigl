import stlConvert
import primitives

cube = stlConvert.fromSTL("centered_cubetest.stl")
icos = primitives.icosphere(1, [0,0,0])#stlConvert.fromSTL("hires_ico.stl")


import renderer
import rotation
import perspective
import shading
import math

anim = []

degIncrement = 3
fullRotation = 360


for angle in range(0, fullRotation, degIncrement):
    rotatedFrame = []
    for polygon in cube:
        rotatedPolygon = []
        #print(polygon)
        for vertex in polygon:
                            
            vertex = vertex + [0]
            
            rotatedVertex = rotation.x(math.radians(2*angle), vertex)
            rotatedVertex = rotation.y(math.radians(angle), rotatedVertex)
            
            vertex = vertex[:-1]
            
            # MOVE ROTATED VERTEX INTO CAMERA AREA, SO ADD 2.5 TO EVERY AXIS
            
            translatedVertex = []
            
            x,y,z = rotatedVertex[:3]
            
            translatedVertex.append(5 + x)
            translatedVertex.append(2.5 + y)
            translatedVertex.append((math.sin(math.radians(angle * 2)) * 4)  + 5 + z)


            rotatedPolygon.append(translatedVertex)
            
        # calculate lighting
        v0,v1,v2,v3 = rotatedPolygon
        lightSources = [[0,0,1]]

        lightness = shading.getLightness(v0,v1,v2,lightSources[0])
        
        rotatedPolygon.append(lightness)
            
        rotatedFrame.append(rotatedPolygon)
        
    for polygon1 in icos.originPolygons:
        rotatedPolygon = []
        #print(polygon)
        for vertex in polygon1:
                            
            vertex = vertex + [0]
            
            rotatedVertex = rotation.y(math.radians(angle * 2), vertex)
            #rotatedVertex = rotation.x(math.radians(angle), rotatedVertex)
            
            vertex = vertex[:-1]
            
            # MOVE ROTATED VERTEX INTO CAMERA AREA, SO ADD 2.5 TO EVERY AXIS
            
            translatedVertex = []
            
            for i in rotatedVertex[:2]:
                translatedVertex.append(2.5 + i)
            z = rotatedVertex[2]
            translatedVertex.append(0 + z)
            
            rotatedPolygon.append(translatedVertex)
        
        # calculate lighting
        v0,v1,v2 = rotatedPolygon
        lightSources = [[0,0,1]]

        lightness = shading.getLightness(v0,v1,v2,lightSources[0])
        
        rotatedPolygon.append(lightness)
        
        rotatedFrame.append(rotatedPolygon)
        
    anim.append(rotatedFrame)

anim = perspective.allFrames(anim)

renderer.playAnim(anim, 360, 90, 60)
