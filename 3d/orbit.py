import primitives
import renderer
import perspective
import rotation
import numpy as np
import shading

from astropy import units as u
from astropy.coordinates import CartesianRepresentation

from poliastro.bodies import Earth
from poliastro.twobody import Orbit
from poliastro.examples import iss
from pyfiglet import figlet_format

earthZ = 1
earthR = 1.5
icoOrigin = [5,2.5,earthZ]

earth = primitives.icosphere(earthR, [5,2.5,earthZ])

satellite = primitives.cube(0.25, [5,2.5,1])

fps = 60
lps = 0.06125

anim = []

# NOAA 15 [B]             
# 1 25338U 98030A   18282.92316395  .00000024  00000-0  28859-4 0  9992
# 2 25338  98.7660 299.4120 0009462 252.4277 107.5869 14.25878343 61116

#g = 3.986004418 * (10**14)
#n = 14.25878343
#n = ((np.pi * n * 2)/86400)**(2/3)
#print(n)
#a = (g**1/3)/n * u.km
#ecc = 0.009462 * u.one
#inc = 98.7660 * u.deg
#raan = 299.4120 * u.deg
#argp = 252.4277 * u.deg
#nu = 107.5869 * u.deg

#noaa = Orbit.from_classical(Earth, a, ecc, inc, raan, argp, nu)

#print(noaa)

def getCoords(orbit, numSpaces):
    cart = orbit.represent_as(CartesianRepresentation).xyz / u.km
    x, z, y = cart
    x = float(x)
    y = float(y)
    z = float(z)

    # currently relative to center of earth (icosphere)
    # need to be turned into coords of plane, with limits of 1.5 * earthR
    maxDist = float(orbit.state.a / u.km)
    x = x * ((1.5*earthR)/maxDist)
    y = y * ((1.5*earthR)/maxDist)
    z = z * ((1.5*earthR)/maxDist)
    
    # make relative to origin
    x0, y0, z0 = icoOrigin
    x = x + x0
    y = y + y0
    z = z + z0
    print((" " * numSpaces) + str((x,y,z)))
    
    return [x,y,z]
    
cubeLocs = []


r = [-3425, 6023, 604] * u.km
v = [-7.337,-3.495,1.294]* u.km / u.s
orbit = Orbit.from_vectors(Earth,r,v)

opd = 1440 / (float(orbit.period/u.s) / 60)

for angle in np.linspace(0, np.pi*2, num=fps/lps/opd*2):
    scalar = 1440/(np.pi*2)/opd
    
    orbitNew = orbit.propagate(angle * scalar * u.min)
    
    numSpaces = int((np.sin(angle) + 1) * 125)
    
    cubeLoc = getCoords(orbitNew, numSpaces)
    cubeLocs.append(cubeLoc)

cubeLocCounter = 0
trailLength = 30
    
for angle in np.linspace(0, np.pi*2, num=fps/lps/opd*2):
    
    satellite.move(cubeLocs[cubeLocCounter])

    earth.rotY(-(np.pi*2/(fps/lps)))
    
    tmpFrame = []
    
    numSpaces = int((np.sin(angle) + 1) * 125)
    
    for i in range(trailLength):
        print((" " * numSpaces) + str(cubeLocs[cubeLocCounter - i]))
        tmpFrame.append([cubeLocs[cubeLocCounter - i]])
    
    cubeLocCounter += 1
    
    for polygon in satellite.originPolygons:
        # calculate lighting
        v0,v1,v2,v3 = polygon
        lightSources = [[10,0,1]]

        lightness = shading.getLightness(v0,v1,v2,lightSources[0])
        
        polygon.append(lightness)
            
        tmpFrame.append(polygon)
        
        
    for polygon in earth.originPolygons:
        # calculate lighting
        v0,v1,v2 = polygon
        lightSources = [[0,0,1]]

        lightness = shading.getLightness(v0,v1,v2,lightSources[0])
        
        polygon.append(lightness)
        
        tmpFrame.append(polygon)
    
    anim.append(tmpFrame)
    
print(figlet_format("RENDERING"))
anim = perspective.allFrames(anim)

renderer.playAnim(anim, 360, 90, fps, "orbit.txt")
