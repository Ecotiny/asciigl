# input array
# [ [ [[2,3,2],[4,7,3]] ] ]
# | | |____face/line__| | |
# | |_____frame_________| |
# |_____animation_________|

# default camera plane from [0,0,0] to [5,5,0] (maybe changable later)

# O_____________________o

import collections

testInput = [ [ [[1,4,1],[2,3,1],[1,2,1]], [[2.5,3,2],[1.5,1,2],[0.5,4,3]] ] ]

# TODO: vector transform to align the camera
# TODO: z clipping/clipping in general
# TODO: n-gons

# order polygons and vertices by z, then project to camera to prepare for 2d vector rendering

def orderFrame(enput): # god dam austrinesians
    # take average z of polygons then order by that
    avg_z = {}
    for polygon_index in range(len(enput)):
        polygon = enput[polygon_index]
        
        #print(str(enput[polygon_index]) + "\n")
        
        allz = 0
        
        for vertex in polygon:

            allz += vertex[2]
            
        avgz = allz / len(polygon)
        
        avg_z[polygon_index] = avgz
           
    sorted_avgz = collections.OrderedDict(sorted(avg_z.items(), key=lambda kv: kv[1])[::-1]) # order dict biggest to smallest
        
    output = []
    
    for polygon_index, _ in sorted_avgz.items():
        polygon = enput[polygon_index]
        
        outPolygon = []
        
        for vertex in polygon:
            x,y,z = vertex
            
            outPolygon.append([x,y])
            
        output.append(outPolygon)
        
    return output
        
def allFrames(enput): # god dam austrinesians
    
    orderedFrames = []
    
    print(len(enput[0]))
    
    for frame in enput:
        orderedFrames.append(orderFrame(frame))
        
    print(len(orderedFrames[0]))
    
    return orderedFrames
