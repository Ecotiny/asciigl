import numpy as np
import collections

def project(a):
    
    ax, ay, az = a
    
    ez = 3 # display surface position depth (rel to camera)
    ds = [10,5]
    
    ax = ax - (ds[0]/2)
    ay = ay - (ds[1]/2)
    
    by = ez*(ay/(az + ez))
    bx = ez*(ax/(az + ez))
    
    by = (ds[1] / 2) + by
    bx = (ds[0] / 2) + bx
    
    return [bx,by]
    
def process(frame):
    
    # take average z of polygons then order by that
    avg_z = {}
    for polygon_index in range(len(frame)):
        polygon = frame[polygon_index]
                
        allz = 0
        
        for item in polygon:
            if type(item) == list:
                allz += item[2]
            
        avgz = allz / len(polygon)
        
        avg_z[polygon_index] = avgz
           
    sorted_avgz = collections.OrderedDict(sorted(avg_z.items(), key=lambda kv: kv[1])[::-1]) # order dict biggest to smallest
    
    output = []
    
    for polygon_index, _ in sorted_avgz.items():
        polygon = frame[polygon_index]
        
        outPolygon = []
        
        for item in polygon:
            if type(item) == list:
                x,y = project(item)
                
                outPolygon.append([x,y])
            else:
                outPolygon.append(item)
            
        output.append(outPolygon)
        
    return output
    
    
    
def allFrames(anim):
        
    orderedFrames = []
        
    for frame in anim:
        orderedFrames.append(process(frame))
        
    print("# of polygons: {}".format(len(orderedFrames[0])))
    
    return orderedFrames
