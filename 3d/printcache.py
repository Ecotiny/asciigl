import subprocess
import time
import argparse

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(description="Display 3d animation")
    parser.add_argument("-fl", "--filename", type=str, default="cache.txt", help="File to print from")
    
    parser.add_argument("-fr", "--fps", type=int, default=60, help="Framerate to run file at")
    
    args = parser.parse_args()
    
    fps = int(args.fps)
    filename = args.filename
    
    with open(filename, "rt") as fl:
        lines = fl.read().split("\n\n")
        frames = lines
        while True:
            for frame in frames:
                subprocess.run("clear")
                print(frame)
                time.sleep(1/fps)
            
