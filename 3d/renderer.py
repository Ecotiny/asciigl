import time
import subprocess
from pyfiglet import figlet_format
    
def plotline(v0, v1):
    x0 = v0[0]
    x1 = v1[0]
    y0 = v0[1]
    y1 = v1[1]
    w = x1 - x0
    h = y1 - y0
    dx1 = 0
    dy1 = 0
    dx2 = 0
    dy2 = 0
    if (w<0):
        dx1 = -1
    elif (w>0):
        dx1 = 1
    
    
    if (h<0):
        dy1 = -1 
    elif (h>0):
        dy1 = 1
    if (w<0):
        dx2 = -1 
    elif (w>0):
            dx2 = 1 
    longest = abs(w)
    shortest = abs(h)
    
    output = [[x0, y0], [x1, y1]]
    
    if not (longest>shortest):
        longest = abs(h)
        shortest = abs(w)
        if (h<0):
            dy2 = -1 
        elif (h>0):
            dy2 = 1
        dx2 = 0     
        
    numerator = (longest-(longest%2))/2
    for i in range(longest + 1):
        output.append([x0,y0])
        numerator += shortest
        if not (numerator<longest):
            numerator -= longest
            x0 += dx1
            y0 += dy1
        else:
            x0 += dx2
            y0 += dy2
            
    return output

def scale(outputWidth, outputHeight, frame):
    cameraSize = [10, 5]
    xfactor = outputWidth/cameraSize[0]
    yfactor = outputHeight/cameraSize[1]
    out = []
    for polygon in frame:
        bowl = []
        for item in polygon:
            if type(item) == list:
                tmp = [int(item[0] * xfactor) , int((cameraSize[1] - item[1]) * yfactor)]
                bowl.append(tmp)            
            else:
                bowl.append(item)
            
        out.append(bowl)
    return out
    
# [[[60.0, 60.0], [36.0, 20.0], [12.0, 80.0]], [[24.0, 80.0], [48.0, 60.0], [24.0, 40.0]]]

def fill(l0, l1, l2, v0, v1, v2): # gets area for face filling
    x = []
    y = []
    
    for v in [v0, v1, v2]:
        x.append(v[0])
        y.append(v[1])
        
    x.sort()
    y.sort()
    
    minX, medX, maxX = x
    minY, medY, maxY = y
    
    originLines = []
    
    # convert lines to bounding box-relative coords
    for line in [l0, l1, l2]:
        bowl = []
        for x, y in line:
            bowl.append([x - minX, y - minY])
        originLines.append(bowl)
        
    output = []
        
    # actually fill
    for y in range(maxY - minY + 1):
        intersections = []
        for x in range(maxX - minX + 1):
            # detect intersection            
            for x1,y1 in originLines[0] + originLines[1] + originLines[2]:
                if x == x1 and y == y1:
                    intersections.append(x)
        
        if len(intersections) > 0:
            for x in range(maxX - minX + 1):
                if x >= min(intersections) and x <= max(intersections):
                    output.append([x,y])
        else:
            print(len(intersections))
            print(v0, v1, v2)
            print(maxY - minY + 1)
            
    # convert back
    newOutput = []
    for x,y in output:
        newOutput.append([x + minX, y + minY])
        
    return newOutput
     
def colour(r,g,b):
    return '\x1b[38;2;{};{};{}m'.format(r,g,b)

def generateArrayFrame(width, height, frame, shading=True):
    
    #lightingGrad = ['$','@','B','%','8','&','W','M','#','o','a','h','k','d','p','q','w','m','z','c','{','}','[',']','?','+','~','<','>','i','l',';',':','"','^','`',"'",'.'," "]
    
    maxColour = colour(235,235,235)
    minColour = colour(50,50,50)
    endchar = '\x1b[0m'

    lightingGrad = [maxColour + '$' + endchar,'&','p','c','i','.', minColour + '.' + endchar]
    
    spacerChar = " "
    lineColour = colour(150,150,150)
    vertColour = colour(127,127,127)
    fillColour = colour(127,127,127)
    fillChar = fillColour + "." + endchar
    lineChar = lineColour + "*" + endchar
    vertChar = "%"#vertColour + "%" + endchar
    frame = scale(width, height, frame)
    
    # faces -> lines -> verts
    
    frameArr = [[spacerChar for y in range(height)] for x in range(width)]
        
    for polygon in frame:
        vertices = []
        lightness = -1.0
        for item in polygon:
            if type(item) == list:
                vertices.append(item)
            else:
                lightness = item
        
        if len(vertices) >= 2: # do the lines
            if len(vertices) == 4: # do rects with 0,2 hidden
                v0 = vertices[0]
                v1 = vertices[1]
                v2 = vertices[2]
                v3 = vertices[3]
                l0 = plotline(v0, v1)
                l1 = plotline(v1, v2)
                l2 = plotline(v2, v3)
                l3 = plotline(v3, v0)
                lh = plotline(v0, v2)
                
                fill1 = fill(l0,l1,lh,v0,v1,v2)
                fill2 = fill(lh,l2,l3,v0,v2,v3)
                
                fillArr = fill1 + fill2
                
                if shading and lightness != -1.0:
                    charIndex = int((1 - lightness) * len(lightingGrad)) - 1
                    tmpFillChar = lightingGrad[charIndex]
                    frameArr = override(frameArr, fillArr, tmpFillChar)
                else:
                    frameArr = override(frameArr, fillArr, fillChar)
                                
                #for line in [l0,l1,l2,l3]:
                    #frameArr = override(frameArr, line, lineChar)
                
            elif len(vertices) == 3:
                v0 = vertices[0]
                v1 = vertices[1]
                v2 = vertices[2]
                line0 = plotline(v0, v1)
                line1 = plotline(v1, v2)
                line2 = plotline(v2, v0)
                
                fillArr = fill(line0, line1, line2, v0, v1, v2)
                
                if shading and lightness != -1.0:
                    charIndex = int((1 - lightness) * len(lightingGrad)) - 1
                    if charIndex == -1:
                        charIndex = 0
                    tmpFillChar = lightingGrad[charIndex]
                    frameArr = override(frameArr, fillArr, tmpFillChar)
                else:
                    frameArr = override(frameArr, fillArr, fillChar)
                
                #for line in [line0, line1, line2]:
                    #frameArr = override(frameArr, line, lineChar)
                    
            elif len(vertices) == 2:
                v0 = vertices[0]
                v1 = vertices[1]
                line = plotline(v0, v1)
                frameArr = override(frameArr, line, lineChar)
                
        for vert in vertices: # do the verts
            frameArr = override(frameArr, [vert], vertChar)
            
    return frameArr
    
def override(paper, pencil, char):
    for coords in pencil:
        x = coords[0]
        y = coords[1]
        if x < 0 or y < 0:
            z = 0
        else:
            try:
                paper[x][y] = char 
            except:
                z = 0
    return paper


def printFrame(frame):
    frameBowl = ""
    for y in range(len(frame[0])):
        lineBowl = ""
        for x in range(len(frame)):
            lineBowl += frame[x][y]
        
        frameBowl += lineBowl + "\n"
        
    return frameBowl

def playAnim(anim, width, height, fps, filename="cache.txt"):
    with open(filename, "at") as fl:
        fl.truncate(0)
        for frame in range(len(anim)):
            print(figlet_format("Frame {} of {}".format(frame, len(anim))))
            frame = anim[frame]
            frameArr = generateArrayFrame(width, height, frame)
            fl.write(printFrame(frameArr) + "\n")
    
    print("DONE")
    
    with open(filename, "rt") as fl:
        lines = fl.read().split("\n\n")
        while True:
            for frame in lines:
                subprocess.run("clear")
                print(frame)
                time.sleep(1/fps)
            
    
