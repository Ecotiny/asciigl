import stlConvert
import renderer
import orthogonal
import perspective
import rotation
import shading
import math

filename = "suzanne_lowres.stl"

polygons = stlConvert.fromSTL(filename)

anim = []

degIncrement = 3
fullRotation = 360

for angle in range(0, fullRotation, degIncrement):
    rotatedFrame = []
    for polygon in polygons:
        rotatedPolygon = []
        #print(polygon)
        for vertex in polygon:
                            
            vertex = vertex + [0]
            
            rotatedVertex = rotation.y(math.radians(angle), vertex)
            #rotatedVertex = rotation.x(math.radians(angle), rotatedVertex)
            
            vertex = vertex[:-1]
            
            # MOVE ROTATED VERTEX INTO CAMERA AREA, SO ADD 2.5 TO EVERY AXIS
            
            translatedVertex = []
            
            for i in rotatedVertex[:2]:
                translatedVertex.append(2.5 + i)
            
            translatedVertex.append(vertex[2] + 2)
            
            rotatedPolygon.append(translatedVertex)
            
        # calculate lighting
        v0,v1,v2 = rotatedPolygon
        lightSources = [[0,0,1]]

        lightness = shading.getLightness(v0,v1,v2,lightSources[0])
        
        rotatedPolygon.append(lightness)
            
        rotatedFrame.append(rotatedPolygon)
    
    anim.append(rotatedFrame)
    
#print(str(anim) + "\n" )

anim = perspective.allFrames(anim)

#print(anim)

renderer.playAnim(anim, 360, 90, 60)
