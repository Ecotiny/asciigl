import numpy as np

def y(theta, vertex):
    
    vertex = np.array(vertex)
    
    matrix = np.array([[np.cos(theta), 0, np.sin(theta), 0],
                       [0,             1, 0,             0],
                       [-np.sin(theta),0, np.cos(theta), 0],
                       [0,             0, 0,             1]])
    
    return matrix.dot(vertex)

def x(theta, vertex):
    
    vertex = np.array(vertex)

    matrix = np.array([[1, 0,             0,              0],
                       [0, np.cos(theta), -np.sin(theta), 0],
                       [0, np.sin(theta), np.cos(theta),  0],
                       [0, 0,             0,              1]])
    
    return matrix.dot(vertex)

def z(theta, vertex):

    vertex = np.array(vertex)

    matrix = np.array([[np.cos(theta), -np.sin(theta), 0, 0],
                       [np.sin(theta), np.cos(theta),  0, 0],
                       [0,             0,              1, 0],
                       [0,             0,              0, 1]])
        
    return matrix.dot(vertex)
