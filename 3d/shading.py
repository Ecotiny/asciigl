import numpy as np
import scipy.linalg

def get_theta(vect_a, vect_b):      # Difference in angle between two unit vectors

    dp = np.dot(vect_a, vect_b)
    theta = np.arccos(dp)
        
    return np.degrees(theta)

def get_norm(v1,v2,v3):                    # Returns normal vector as np array
    
    v13 = v3 - v1
    v12 = v2 - v1
    
    normal_vector = np.cross(v13,v12)
    
    return normal_vector

def normalise(vect):
    length = scipy.linalg.norm(vect)
    
    return vect / length

def getLightness(v0,v1,v2,vl):
    
    v0 = np.array(v0)
    v1 = np.array(v1)
    v2 = np.array(v2)
    
    norm = get_norm(v0,v1,v2)
    norm = normalise(norm)
    
    vl = np.array(vl)
    light = normalise(vl)

    angle = get_theta(light, norm)
    # between 0 and 180
    # want between 1 and 0
    light = 0
    if angle < 90:
        angle = angle / 90
        light = 1 - angle
        if angle * 90 < 1:
            print("Oh my godfreys")
            print(light)
    
    return light
