import primitives
import perspective
import renderer
import numpy as np

numCubes = 7
cubeLen = 0.75

cubes = []

for angle in np.linspace(0, 2*np.pi, num=numCubes):
    x = (4 * np.sin(angle)) + 5
    y = (2 * np.sin(2*angle)) + 2.5
    z = (2 * np.sin(4*angle)) + 2
    tmpCube = primitives.cube(cubeLen, [x,y,z])
    cubes.append(tmpCube)
    
loopTime = 2
fps = 60
    
anim = []
    
for angle in np.linspace(0, 2*np.pi, num=loopTime*fps):
    frame = []
    for cube_index in range(numCubes):
        offset = cube_index * ((2*np.pi)/numCubes)
        cube = cubes[cube_index]
        
        cube.rotX((2*np.pi)/(loopTime*fps))
        cube.rotY((2*np.pi)/(loopTime*fps))
        
        x = (4 * np.sin(angle + offset)) + 5
        y = (2 * np.sin(2*angle + offset)) + 2.5
        z = (2 * np.sin(4*angle + offset)) + 2

        cube.move([x,y,z])
        for polygon in cube.originPolygons:
            frame.append(polygon)
        
    anim.append(frame)

print(anim)
anim = perspective.allFrames(anim)
renderer.playAnim(anim, 372, 93, fps, "sinfinity.txt")
