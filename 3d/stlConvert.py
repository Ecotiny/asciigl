import math

def fromSTL(filename):
    with open(filename, "rt") as fl:
        lines = fl.readlines()
        
        inFacet = False
        
        polygons = []

        vertices = []
        for line in lines:
            if "outer loop" in line:
                inFacet = True
            elif "endloop" in line:
                inFacet = False
                polygons.append(vertices)
                vertices = []
            elif "vertex" in line:
                strVertices = line.split(" ")
                del strVertices[0]
                
                verticesNew = []
                            
                for i in strVertices:
                    if "\n" in i:
                        i = i[:-2]
                    verticesNew.append(float(i))
                
                vertices.append(verticesNew)
        
        return polygons
