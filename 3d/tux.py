import stlConvert

tux = stlConvert.fromSTL("tux.stl")


import renderer
import rotation
import perspective
import shading
import math
import numpy as np

anim = []

fps = 60
lps = 1


for angle in np.linspace(0, 360, num=fps/lps):
    rotatedFrame = []
    for polygon in tux:
        rotatedPolygon = []
        #print(polygon)
        for vertex in polygon:
                            
            vertex = vertex + [0]
            
            rotatedVertex = rotation.x(math.radians(90), vertex)
            rotatedVertex = rotation.z(math.radians(180), rotatedVertex)
            rotatedVertex = rotation.y(math.radians(angle), rotatedVertex)
            rotatedVertex = rotation.x(math.radians(-10), rotatedVertex)

            vertex = vertex[:-1]
            
            # MOVE ROTATED VERTEX INTO CAMERA AREA, SO ADD 2.5 TO EVERY AXIS
            
            translatedVertex = []
            
            x,y,z = rotatedVertex[:3]
            
            translatedVertex.append(5 + x)
            translatedVertex.append(2.5 + y)
            translatedVertex.append(z)


            rotatedPolygon.append(translatedVertex)
            
        # calculate lighting
        v0,v1,v2 = rotatedPolygon
        lightSources = [[1,-1,2]]

        lightness = shading.getLightness(v0,v1,v2,lightSources[0])
        
        rotatedPolygon.append(lightness)
            
        rotatedFrame.append(rotatedPolygon)
    anim.append(rotatedFrame)
        

anim = perspective.allFrames(anim)

with open("tux.txt", "wt") as fl:
    fl.write(str(anim))

renderer.playAnim(anim, 360, 90, 60)
