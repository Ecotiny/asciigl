# Example top-level array (can be floats / in 16:9 coords)
# [[[[[x,y,r,g,b],[x,y,r,g,b]],[[x,y,r,g,b],[x,y,r,g,b]]], z],[[[[x,y],[x,y]],[[x,y],[x,y]]], z]]
#          |___|
#           opt
#                f1                       f2               z
# |________________________element 1________________________|

# Intermediate data format
# [[x,y,z], [x,y,z], [x,y,z]]

# Example array (ints / scaled) input into genFrame() for each frame
# {z1: [[x1,y1,r,g,b], [x2,y2]], z2: [[x3,y3], [x4,y4]]}

# 1 is lowest z val

import collections
import time
import subprocess

spacerChar = " "
padding = 15 # as a percentage
paddingDec = padding/100

def tToLCoords(xy):
    
    x = xy[0]
    y = xy[1]
    
    y = 9 - y
    
    return [x, y] # array indices start at 0

def tToLScale(xy, height, width):
    
    # theo's thingy is 16 by 9
    
    x = xy[0]
    y = xy[1]
     
    givenWInc = (width - ((width * paddingDec) + 1))/16
    givenHInc = (height - ((height * paddingDec) + 1))/9
    
    centreW = (width * paddingDec)/2
    centreH = (height * paddingDec)/2
    
    x = x * givenWInc + centreW 
    y = y * givenHInc + centreH 
    
    x = int(x)
    y = int(y)
    
    return [x,y]

def theoArrayToLinusArray(theoArray, height, width):
    # theo's array has a range of 16 > x > 0
    #                             09 > y > 0
    
    numElements = len(theoArray)
    elements = []
    
    numFrames = len(theoArray[0][0])
    
    framesArr = [{} for i in range(numFrames)]
    
    for i in theoArray:
        elements.append(i[1])
    
    for j in range(len(framesArr)):
        for i in elements:
            framesArr[j][i] = []
    
    for element in theoArray:
        # [[[x,y], [x,y]], z]
        zVal = element[1]
        frames = element[0]
        for frameNum in range(numFrames):
            # [[x,y], [x,y]]
            for xy in frames[frameNum]:
                # [x,y, r,g,b]

                if len(xy) not in [2, 5]:
                    raise ValueError("Expected either xy coordinates or xy coordinates with rgb, found something else.")
                
                xycoords = tToLCoords(xy[:2])
                xycoords = tToLScale(xycoords, height, width)
                newxy = xycoords + xy[2:]
                
                xy = newxy
                                
                framesArr[frameNum][zVal].append(xy)
                
    return framesArr

def getChar(z, rgb=None):
    chars = ["I", "B", "$"]
    outstr = chars[z-1] # array indices start at 0, z val doesn't
    if len(rgb) == 3:
        r = rgb[0]
        g = rgb[1]
        b = rgb[2]
        colour = '\x1b[38;2;{};{};{}m'.format(r, g, b)
        endchar = '\x1b[0m'
        outstr = colour + outstr + endchar
    return outstr

def genFrame(inDict, height, width, border=False):
    frame = [[spacerChar for y in range(height)] for x in range(width)]
    
    sortedDict = collections.OrderedDict(sorted(inDict.items()))
    
    if border:

        corner = [[0,0], [width-1, height-1], [0, height-1], [width-1, 0]]

        for x in range(width):
            frame[x][0] = "-"
            frame[x][height-1] = "-"
        for y in range(height):
            frame[0][y] = "|"
            frame[width-1][y] = "|"
        for xy in corner:
            x = xy[0]
            y = xy[1]
            frame[x][y] = "+"
    
    for z, xyArr in sortedDict.items():
        for xy in xyArr:
                        
            x = xy[0]
            y = xy[1]
            
            char = getChar(z, xy[2:])
                        
            
            if "I" in char: # do ball
                # stored images in dict
                # relative x and y from centre to value
                trail = {"-1,0": ":", '1,0': ":", '0,-1': ":", '0,1': ":"}
                
                colour, endchar = ["",""]
                
                if len(char) > 2:
                    r,g,b = xy[2:]
                    colour = '\x1b[38;2;{};{};{}m'.format(r, g, b)
                    endchar = '\x1b[0m'
                
                #  :
                # : :
                #  :
                
                for k, v in trail.items():
                    k = k.split(",")
                    relX = int(k[0])
                    relY = int(k[1])
                    frame[x + relX][y + relY] = colour + v + endchar
            elif "$" in char:
                
                colour, endchar = ["",""]
                
                if len(char) > 2:
                    r,g,b = xy[2:]
                    colour = '\x1b[38;2;{};{};{}m'.format(r, g, b)
                    endchar = '\x1b[0m'
                
                # remember coords are weird, negative y is up
                
                ball = {"0,0" : " ", "1,0" : " ", "2,0" : ")", "-1,0" : " ", "-2,0" : "(", "0,1" : ".", "1,1" : "/", "-1,1" : "\\", "0,-1" : "*", "1,-1" : "\\", "-1,-1" : "/"}
                #  /*\
                # (   )
                #  \_/
                for k, v in ball.items():
                    k = k.split(",")
                    relX = int(k[0])
                    relY = int(k[1])
                    frame[x + relX][y + relY] = colour + v + endchar
            elif "B" in char:
                
                ball_sprite_1 = {'-1,-2': 'd', '0,-2': '8', '1,-2': 'b', '-2,-1': 'd', '-1,-1': '8', '0,-1': '8', '1,-1': '8', '2,-1': 'b', '-3,0': '(', '-2,0': '8', '-1,0': '8', '0,0': '8', '1,0': '8', '2,0': '8', '3,0': ')', '-2,1': 'Y', '-1,1': '8', '0,1': '8', '1,1': '8', '2,1': 'Y', '-1,2': 'Y', '0,2': '8', '1,2': 'Y'}
                
                #ball_sprite_2 = {'-1,-1': 'd', '0,-1': '8', '1,-1': 'b', '-3,0': '(', '-2,0': '8', '-1,0': '8', '0,0': '8', '1,0': '8', '2,0': '8', '3,0': ')', '-1,1': 'Y', '0,1': '8', '1,1': 'Y'}

                colour, endchar = ["",""]
                
                if len(char) > 2:
                    r,g,b = xy[2:]
                    colour = '\x1b[38;2;{};{};{}m'.format(r, g, b)
                    endchar = '\x1b[0m'
                
                for k, v in ball_sprite_1.items():
                    k = k.split(",")
                    relX = int(k[0])
                    relY = int(k[1])
                    frame[x + relX][y + relY] = colour + v + endchar
                
            else:
                frame[x][y] = getChar(z, xy[2:])
            
            
    return frame

def frameToString(frame):
    frameBowl = ""
    for y in range(len(frame[0])):
        lineBowl = ""
        for x in range(len(frame)):
            lineBowl += frame[x][y]
        
        frameBowl += lineBowl + "\n"
        
    return frameBowl

running = False

def stop():
    running = False
    print("STOPPING")

def playAnim(theoArray, height, width, fps):
    linusArr = theoArrayToLinusArray(theoArray, height, width)
    
    running = True
    
    while running:
        if not running:
            break
        for frame in linusArr:
            #subprocess.run("clear")
            subprocess.run("clear")
            frameArr = genFrame(frame, height, width, True)
            print(frameToString(frameArr))
            time.sleep(1/fps)
