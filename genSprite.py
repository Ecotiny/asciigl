filename = "ball_sprite_2.txt"

cleaning_chars = ["\n", "\t"]
excluded_chars = [" "]

with open(filename, 'rt') as fl:
    lines = fl.readlines()
    maxWidth = 0
    
    outDict = {}
    
    newLines = []
    
    # clean lines of \n and \t
    for line in lines:
        lineBowl = ""
        for char in line:
            if char not in cleaning_chars:
                lineBowl += char
                
        newLines.append(lineBowl)
        
    lines = newLines
    
    for line in lines:
        if len(line) > maxWidth:
            maxWidth = len(line)
    if len(lines) % 2 == 0 or maxWidth %2 == 0:
        raise Exception("Sprite must have odd dimensions to determine centre")
    else:
        center = (int(maxWidth/2), int(len(lines)/2))
        print(center)
        for line_index in range(len(lines)):
            relY = line_index - center[1]
            line = lines[line_index]
            for char_index in range(len(line)):
                char = line[char_index]
                if char not in excluded_chars:
                    relX = char_index - center[0]
                    outDict["{},{}".format(relX, relY)] = char
                    
    print("{} = {}".format(filename[:-4], outDict))
