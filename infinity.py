# composed of a cosine motion going from bottom left to top right, then a semicircle going to another cosine, but bottom right to top left, then a semicircle connecting the two

import math
import frameGen

coords = []

fps = 60
trailZ = 1

cosineFrames = 20
circleFrames = 20

width = 144
height = 37

colour = [255, 100, 255]

# first cosine
for i in range(cosineFrames): # 60 frames for the first cosine
    #x = -(3.5 * (math.cos(i * (math.pi/cosineFrames)))) + 8 # to center it
    # linear instead of consine
    x = ((7/cosineFrames) * i) + 4.5
    y = x - 3.5 # gradient of 1
    
    coords.append([[x,y] + colour])
    
angleInc = (math.pi)/circleFrames

# first circle
for angle in range(circleFrames): # 30 frames for the first circle
    angle = angle * angleInc
    
    # handle edge cases first
    if angle == 0:
        relY = 3.5
        relX = 0
    elif angle == math.pi/2:
        relY = 0
        relX = 3.5
    elif angle == math.pi:
        relY = -3.5
        relX = 0
    elif angle < (math.pi/2): # in first quadrant
    
        # hypotenuse = radius = 3.5
        relX = 3.5 * math.sin(angle)
        relY = 3.5 * math.cos(angle)
        
    else: # in second quadrant
        
        angle = angle - (math.pi/2) # to get to relative angle
        
        relX = 3.5 * math.cos(angle)
        relY = -(3.5 * math.sin(angle))
    
    x = relX + 11.5
    y = relY + 4.5
    
    coords.append([[x,y] + colour])
    
# second cosine
for i in range(cosineFrames): # 60 frames for the second cosine
    #x = 3.5 * (math.cos(i * (math.pi/cosineFrames))) + 8 # to center it
    # linear instead of consine
    x = ((7/cosineFrames) * -i) + 11.5
    y = -x + 12.5 # gradient of -1
    
    coords.append([[x,y] + colour])
    
# second circle
# first circle
for angle in range(circleFrames): # 30 frames for the first circle
    angle = angle * angleInc
    
    # handle edge cases first
    if angle == 0:
        relY = 3.5
        relX = 0
    elif angle == math.pi/2:
        relY = 0
        relX = 3.5
    elif angle == math.pi:
        relY = -3.5
        relX = 0
    elif angle < (math.pi/2): # in first quadrant
    
        # hypotenuse = radius = 3.5
        relX = 3.5 * math.sin(angle)
        relY = 3.5 * math.cos(angle)
        
    else: # in second quadrant
        
        angle = angle - (math.pi/2) # to get to relative angle
        
        relX = 3.5 * math.cos(angle)
        relY = -(3.5 * math.sin(angle))
    
    x = 4.5 - relX
    y = relY + 4.5
    
    coords.append([[x,y] + colour])

def genTrail(coords, length):
    outBowl = []
    for frame in range(len(coords)):
        bowl = []
        for n in range(length):
            #coords[frame][0] # current coord at frame
            greyscale = 255 - int((255/length)*n) # fade as further away from start
            for tmpCoords in coords[frame-n]:
                currgb = tmpCoords[2:]
                
                tmpCoords = tmpCoords[:2]
                
                newrgb = []
                r,g,b = [greyscale, greyscale, greyscale]
                if len(currgb) > 0: # used to retain prev colour
                    for c in currgb:
                        if c != 255:
                            newrgb.append(c)
                        else:
                            newrgb.append(greyscale)
                    r,g,b = newrgb
                bowl.append(tmpCoords + [r, g, b])
        outBowl.append(bowl)
        
    return [outBowl, trailZ]

trail = genTrail(coords, 15)
coords = [coords, 2]

arr = [coords, trail]

frameGen.playAnim(arr, height, width, fps)
