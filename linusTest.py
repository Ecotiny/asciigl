# create x vals
# create y vals as a function of x

import math
import frameGen

fps = 60
loop_time = 1
coords = []

trailZ = 1

for i in range(fps*loop_time):
    # main sine wave
    x = 8*((math.sin((2*math.pi*i)/fps)))
    x += 8
    # cosine wave
    x1 = 8*((math.cos((2*math.pi*i)/fps)))
    x1 += 8
    
    # replacing x with x1 in this line gives circle (ovalish)
    y = -(x * (9/16)) + 9

    # replacing x1 with x in this line gives circle (ovalish)
    y1 = x1 * (9/16)
    
    #x2 = 8*((math.cos(((2*math.pi*i) + math.radians(10))/fps))) # out of phase
    #x2 += 8
    ## replacing x2 with x1 in this next line gives circle
    #y2 = x * (9/16)
    
    bowl = [[x, y, 255, 0, 124], [x1,y1, 0, 124, 255]]
    coords.append(bowl)
    
arr = [[coords, 2]]

trail = []

def genTrail(coords, length):
    outBowl = []
    for frame in range(len(coords)):
        bowl = []
        for n in range(length):
            #coords[frame][0] # current coord at frame
            greyscale = 255 - int((255/length)*n) # fade as further away from start
            for tmpCoords in coords[frame-n]:
                currgb = tmpCoords[2:]
                
                tmpCoords = tmpCoords[:2]
                
                newrgb = []
                r,g,b = [greyscale, greyscale, greyscale]
                if len(currgb) > 0: # used to retain prev colour
                    for c in currgb:
                        if c != 255:
                            newrgb.append(c)
                        else:
                            newrgb.append(greyscale)
                    r,g,b = newrgb
                bowl.append(tmpCoords + [r, g, b])
        outBowl.append(bowl)
        
    return [outBowl, trailZ]
    
arr.append(genTrail(coords, 10))

frameGen.playAnim(arr, 81, 144, fps)
