
import math
import numpy as np
import frameGen
import time

fps = 120
trailZ = 1
secPerLoop = 10
trailLength = 60
numBalls = 11
height, width = [107, 379]
allWhite = True

colours = []

maxH = 0

if allWhite:
    for i in range(numBalls):
        colours.append([255,255,255])
else:
    for i in range(numBalls):
        colours.append(list(np.random.choice(range(256), size=3)))

coords = []

def getXY(angle, offset, colour, maxH):
    
    y = 4.5 * math.cos(11 * (angle + offset)) + 4.5 # to centre it
    x = 8 * math.sin(12 * angle + offset) + 8 # to centre it
    
    if y > maxH:
        maxH = y
    
    return (maxH, [x, y] + colour)
    
    
for i in range(int(fps*secPerLoop)):
    angle = ((2*math.pi)/(fps*secPerLoop)) * i
    
        
    incr = (2*math.pi)/numBalls
    
    frameBowl = []
    
    for n in range(numBalls):
        maxH, xy = getXY(angle, incr * n, colours[n], maxH)
        frameBowl.append(xy)
    
    coords.append(frameBowl)
    
arr = [[coords, 2]]


def genTrail(coords, length):
    outBowl = []
    for frame in range(len(coords)):
        bowl = []
        for n in range(length):
            n += 1 # array indices start at 0
            #coords[frame][0] # current coord at frame
            greyscale = 255 - int((255/length)*n) # fade as further away from start
            for tmpCoords in coords[frame-n]:
                currgb = tmpCoords[2:]
                
                tmpCoords = tmpCoords[:2]
                
                newrgb = []
                r,g,b = [greyscale, greyscale, greyscale]
                if len(currgb) > 0: # used to retain prev colour
                    for c in currgb:
                        cInc = (c/length)
                        newC = c - (cInc * n)
                        
                        newrgb.append(int(newC))
                        
                    r,g,b = newrgb
                bowl.append(tmpCoords + [r, g, b])
        outBowl.append(bowl)
        
    return [outBowl, trailZ]

arr.append(genTrail(coords, trailLength))
frameGen.playAnim(arr, height, width, fps)
print(maxH)
