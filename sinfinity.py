
import math
import numpy as np
import frameGen
import time

def playSinfinity(fps, trailZ, secPerLoop, trailLength, numBalls, height, width, allWhite):

    colours = []
    
    maxH = 0

    if allWhite:
        for i in range(numBalls):
            colours.append([255,255,255])
    else:
        for i in range(numBalls):
            colours.append(list(np.random.choice(range(256), size=3)))

    coords = []

    #colour = [255,0,102]
    #colour1 = [102,0,255]
    #colour2 = [0,54,255]
    #colour3 = [255,12,255]
    #colour = [255, 0, 255]
    #colour1 = [255, 255, 0]
    #colour2 = [0, 255, 255]
    #colour3 = [255, 0, 0]
    #colour4 = [0, 255, 0]

    #colours = [[255,0,102], [102, 0, 255], [0, 54, 255], [255, 12, 255], [255, 0, 255], [255,0,102], [102, 0, 255], [0, 54, 255], [255, 12, 255], [255, 0, 255], [255, 0, 255]]

    def getInfinityXY(angle, offset, colour):
        
        y = 4.5 * math.sin(2 * (angle + offset)) + 4.5 # to centre it
        x = 8 * math.sin(angle + offset) + 8 # to centre it
        
        return [x, y] + colour
        
        
    for i in range(int(fps*secPerLoop)):
        angle = ((2*math.pi)/(fps*secPerLoop)) * i
        
            
        incr = (2*math.pi)/numBalls
        
        frameBowl = []
        
        for n in range(numBalls):
            xy = getInfinityXY(angle, incr * n, colours[n])
            if xy[1] > maxH:
                maxH = xy[1]
            frameBowl.append(xy)
        
        coords.append(frameBowl)
        
        #y = 4.5 * math.sin(2 * angle) + 4 # to centre it
        #x = 8 * math.sin(angle) + 8 # to centre it
        
        #y1 = 4.5 * math.sin(2 * (angle + incr)) + 4 # to centre it
        #x1 = 8 * math.sin(angle + incr) + 8 # to centre it
        
        #y2 = 4.5 * math.sin(2 * (angle + (2 * incr))) + 4 # to centre it
        #x2 = 8 * math.sin(angle + (2 * incr)) + 8 # to centre it
        
        #y3 = 4.5 * math.sin(2 * (angle + (3 * incr))) + 4 # to centre it
        #x3 = 8 * math.sin(angle + (3 * incr)) + 8 # to centre it
        
        #y4 = 4.5 * math.sin(2 * (angle + (4 * incr))) + 4 # to centre it
        #x4 = 8 * math.sin(angle + (4 * incr)) + 8 # to centre it
        
        #coords.append([[x,y] + colour, [x1, y1] + colour1, [x2, y2] + colour2, [x3, y3] + colour3, [x4, y4] + colour4])
        
    arr = [[coords, 2]]


    def genTrail(coords, length):
        outBowl = []
        for frame in range(len(coords)):
            bowl = []
            for n in range(length):
                n += 1 # array indices start at 0
                #coords[frame][0] # current coord at frame
                greyscale = 255 - int((255/length)*n) # fade as further away from start
                for tmpCoords in coords[frame-n]:
                    currgb = tmpCoords[2:]
                    
                    tmpCoords = tmpCoords[:2]
                    
                    newrgb = []
                    r,g,b = [greyscale, greyscale, greyscale]
                    if len(currgb) > 0: # used to retain prev colour
                        for c in currgb:
                            cInc = (c/length)
                            newC = c - (cInc * n)
                            
                            newrgb.append(int(newC))
                            
                        r,g,b = newrgb
                    bowl.append(tmpCoords + [r, g, b])
            outBowl.append(bowl)
            
        return [outBowl, trailZ]

    arr.append(genTrail(coords, trailLength))
    frameGen.playAnim(arr, height, width, fps)

if __name__ == "__main__":
    import argparse
    
    parser = argparse.ArgumentParser(description="Display an infinity using the addition of two sinewaves, one half the frequency of the other")
    parser.add_argument("-s", "--spl", type=int, default=10, help="Seconds per loop, one ball making it all the way around")
    
    parser.add_argument("-w", "--white", type=int, help="To decide if to randomise or not (0 - randomise, 1 - all white)")

    
    args = parser.parse_args()
    
    trailZ = 1
    fps = 60
    
    secPerLoop = int(args.spl)

    trailLength = 4

    numBalls = 47#25#41

    height, width = [107, 379]

    allWhite = bool(int(args.white))

    negative = False
    
    #while True:
        #if negative:
            #secPerLoop -= 1
        #else:
            #secPerLoop += 1
            
        #if secPerLoop > 15:
            #secPerLoop = 15
            #negative = True
        #elif secPerLoop < 1:
            #secPerLoop = 1
            #negative = False
    
    playSinfinity(fps, trailZ, secPerLoop, trailLength, numBalls, height, width, allWhite)
    

    
