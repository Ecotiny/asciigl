#!/bin/bash
counter=1
negative=false
while :
do
	if [$negative]
	then
		counter=$(( $counter-1 ))
	else
		counter=$(( $counter+1 ))
	fi
	if [$counter -gt 15]
	then
		counter=15
		negative=true
	elif [$counter -lt 1]
	then
		counter=1
		negative=false
	fi
	
	python3 sinfinity.py --spl=$counter
	sleep 1
done
