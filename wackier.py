import basicPlayer
import math

# def playEquation(fps, trailZ, secPerLoop, trailLength, numBalls, height, width, allWhite, getXY):
# getXY(angle, incr * n, colours[n])

def getXY(angle, offset, colour):
    
    angle = angle + offset
    
    x = 6.02 * (math.cos(angle) + ((math.cos(12 * angle))/4) - (math.sin(-28 * angle))/9) + (1.33 * 6.02)
    y = 3.455 * (math.sin(angle) + ((math.sin(12 * angle))/4) - (math.cos(-28 * angle))/9) + (1.36 * 3.455)
    
    return [x,y] + colour

fps = 120
trailZ = 1
secPerLoop = 3
trailLength = 61
numBalls = 6
height, width = [107, 379]
allWhite = False

basicPlayer.playEquation(fps, trailZ, secPerLoop, trailLength, numBalls, height, width, allWhite, getXY)
